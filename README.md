# DLIM-Superresolution



## Importing Environment

A working Conda Environment is provided in liif_conda_env.yml.

To import the environment download this file and run:

conda env create -n {ENVIRONMENT_NAME} -f liif_conda_env.yml

